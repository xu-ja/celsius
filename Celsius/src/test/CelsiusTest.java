package test;

/*
 * Jason Xu 991545529
 * Syst38634 Software Process Management Midterm
 */

import static org.junit.Assert.*;

import org.junit.Test;

import sheridan.Celsius;

public class CelsiusTest {

	@Test
	public void testCelsius() {
		assertTrue("Incorrect fahrenheit to celsius conversion", Celsius.fromFahrenheit(34) == 1);
	}
	
	@Test
	public void testCelsiusException() {
		assertFalse("Incorrect fahrenheit to celsius conversion", Celsius.fromFahrenheit(0) == 0);
	}

	@Test
	public void testCelsiusBoundaryIn() {
		assertTrue("Incorrect fahrenheit to celsius conversion", Celsius.fromFahrenheit(42) == 6);
	}
	
	@Test
	public void testCelsiusBoundaryOut() {
		assertFalse("Incorrect fahrenheit to celsius conversion", Celsius.fromFahrenheit(42) == 5);
	}
}
